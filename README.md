Building: `mvn clean package`
Resulting file `target/stickyrat.jar`
Running: `java -jar target/stickyrat.jar`
Tested and runs on windows. Press enter after each key
Up - s
Down - x
Left - z
Right - c

To save the game use qx, and use the Up or Down keys to switch the options.

Technical details:
Main class StickyRat.java
CLI is the one which draws the outline and fill empty chars inside the buffer area
MenuAction takes care of showing different menu options, and also has a static inner class which is responsible for Loading a Saved Game.
PausedMenuAction is responsible for pausing a game.
Game is paused and resumes by storing the serialised version of the game, which is StatefullGameObject
GameAction holds the actual methods to save and resume the game.
AddPlayer as the name says add the player, it has certain call back methods which is called based on %name or %fight etc.
StatefullGameObject prints all necessary game chars on to the CLI and holds the state of the game.

Java8 simplies programming with FunctionProgramming and lambda.
Which is used in the form of Consumer, and Runnable in the form of callbacks and Streams/map/filter/collect used whereever necessary.

References are from the internet/google, but the base idea for a CLI is pretty much this, however the CLI can be replaced with any Display.
Drawback : Need to hit enter after every keyDownEvent