package ge.rbg.fight;

import java.awt.event.KeyEvent;

public class Controls {
    public static final int UP = 's';
    public static final int DOWN = 'x';
    public static final int LEFT = 'z';
    public static final int RIGHT = 'c';
    public static final int QUIT = 'q';
    public static final int ENTER = KeyEvent.VK_ENTER;
    public static final int BACKSPACE = KeyEvent.VK_DELETE;

    public static enum Direction {
		UP(0, -1),
		DOWN(0, 1),
		LEFT(-1, 0),
		RIGHT(1, 0);

		public final int dx;
		public final int dy;

		Direction(int dx, int dy) {
			this.dx = dx;
			this.dy = dy;
		}
	}
}
