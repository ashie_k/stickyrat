package ge.rbg.fight.window;

import ge.rbg.fight.Controls;
import ge.rbg.fight.display.CLI;
import ge.rbg.fight.domain.AbstractPlayer;
import ge.rbg.fight.state.StatefullGameObjects;

public class MapWindow extends AbstractWindow {
    private static final int SCROLL_THRESHOLD = 5;

    private int viewPortX;
    private int viewPortY;

    private StatefullGameObjects map;
    private StatefullGameObjects.MapView currentView;

    public MapWindow(int windowX, int windowY, int width, int height) {
        super(windowX, windowY, width, height);
        setBorder(1);
    }

    public void setMap(StatefullGameObjects map) {
        this.map = map;
    }

    private char convertTile(byte t) {
        switch (t) {
            case 0:
                return ' ';
            case 1:
                return '\u253c'; // BOX
            case 2:
                return '\u2502'; // │
            case 3:
                return '\u2500'; // ─────
            default:
                return '?';
        }
    }

    @Override
    public void draw(CLI display) {
        super.draw(display);

        StatefullGameObjects.MapView view = getView();
        for (int y = 0; y < view.height(); y++) {
            for (int x = 0; x < view.width(); x++) {
                display.print(windowX + getBorder() + x, windowY + getBorder() + y, convertTile(view.get(x, y)));
            }
        }

        for (AbstractPlayer object : view.getObjects()) {
            drawObject(display, view, object);
        }

        currentView = null; // invalidate
    }

    private void drawObject(CLI display, StatefullGameObjects.MapView view, AbstractPlayer object) {
        display.print(view.viewX(object.getxPos()) + getBorder(), view.viewY(object.getyPos()) + getBorder(), object.getPlayerSymbol());
    }

    private StatefullGameObjects.MapView getView() {
        if (currentView == null) {
            int xEnd = viewPortX + width - 2 * getBorder();
            if (xEnd >= map.getWidth())
                xEnd = map.getWidth();

            int yEnd = viewPortY + height - 2 * getBorder();
            if (yEnd >= map.getHeight())
                yEnd = map.getHeight();

            currentView = map.view(viewPortX, viewPortY, xEnd - viewPortX, yEnd - viewPortY);
        }

        return currentView;
    }

    private int adjust(int x, int min, int max) {
        if (x < min)
            return min;

        if (x > max)
            return max;

        return x;
    }

    private void scroll(Controls.Direction s) {
        int newViewPortX = adjust(viewPortX + s.dx, 0, map.getWidth() - (width - 2 * getBorder()));
        int newViewPortY = adjust(viewPortY + s.dy, 0, map.getHeight() - (height - 2 * getBorder()));

        if (newViewPortX != viewPortX || newViewPortY != viewPortY)
            currentView = null; // invalidate after scroll

        viewPortX = newViewPortX;
        viewPortY = newViewPortY;
    }

    public void scrollWithPlayer(AbstractPlayer player) {
        StatefullGameObjects.MapView view = getView();

        int viewX = view.viewX(player.getxPos());
        int viewY = view.viewY(player.getyPos());

        if (viewX < SCROLL_THRESHOLD)
            scroll(Controls.Direction.LEFT);

        if (viewX > width - SCROLL_THRESHOLD - 1)
            scroll(Controls.Direction.RIGHT);

        if (viewY < SCROLL_THRESHOLD)
            scroll(Controls.Direction.UP);

        if (viewY > height - SCROLL_THRESHOLD - 1)
            scroll(Controls.Direction.DOWN);
    }

    public void centerAt(AbstractPlayer player) {
        viewPortX = Math.max(player.getxPos() - width / 2, 0);
        viewPortY = Math.max(player.getyPos() - height / 2, 0);
    }
}
