package ge.rbg.fight.window;

import ge.rbg.fight.Controls;
import ge.rbg.fight.display.CLI;
import ge.rbg.fight.service.MenuActions;

public class MenuWindow extends TextWindow {
    private static final String SELECTED_PREFIX = "* ";
    private MenuActions.Menu menu;
    private int index;

    private MenuWindow(int windowX, int windowY, int width, int height) {
        super(windowX, windowY, width, height);
    }

    public MenuWindow(MenuActions.Menu initial) {
        this(0, 0, 5, 5);
        setBorder(1);
        setMenu(initial);
    }

    public void onKeyDown(int key) {
        switch (key) {
            case Controls.UP:
                index = (index + menu.size() - 1) % menu.size();
                break;

            case Controls.DOWN:
                index = (index + 1) % menu.size();
                break;

            case Controls.ENTER:
                MenuActions.Menu.Option option = menu.getChoices().get(index);
                if (option.next != null)
                    setMenu(option.next.get());
                else
                    option.action.run();
                break;
        }
    }

    private void setMenu(MenuActions.Menu m) {
        assert(!m.getChoices().isEmpty());

        this.menu = m;
        this.width = m.getChoices().stream().mapToInt(o -> o.title.length()).max().getAsInt() * 2
                + getBorder() * 2
                + getPadding() * 2
                + SELECTED_PREFIX.length();

        this.height = m.getChoices().size()
                + getBorder() * 2
                + getPadding() * 2;

        this.index = 0;
    }

    @Override
    public void draw(CLI display) {
        this.windowX = (display.getWidth() - width) / 2;
        this.windowY = (display.getHeight() - height) / 2;

        super.clear();
        for (int i = 0; i < menu.getChoices().size(); i++) {
            MenuActions.Menu.Option option = menu.getChoices().get(i);
            boolean selected = i == index;
            super.append(String.format("%3s%s\n", selected ? SELECTED_PREFIX : "", option.title));
        }

        super.draw(display);
    }
}
