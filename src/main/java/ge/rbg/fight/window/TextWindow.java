package ge.rbg.fight.window;

import ge.rbg.fight.display.CLI;

public class TextWindow extends AbstractWindow {
    StringBuffer text = new StringBuffer();

    public TextWindow(int windowX, int windowY, int width, int height) {
        super(windowX, windowY, width, height);
        setPadding(1);
        setBorder(1);
    }

    public void setText(String text) {
        this.text = new StringBuffer(text);
    }

    public void append(String text) {
        this.text.append(text);
    }

    public void deleteLast(int i) {
        text.delete(Math.max(0, text.length() - i), text.length());
    }

    void clear() {
        text = new StringBuffer();
    }

    @Override
    public void draw(CLI display) {
        assert (width < display.getWidth());
        assert (height < display.getHeight());

        display.fillArea(windowX, windowY, windowX + width - 1, windowY + height - 1);

        super.draw(display);

        int delta = getBorder() + getPadding();
        int x = delta;
        int y = delta;

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c == '\n') {
                y++;
                x = delta;
            } else {
                display.print(windowX + x++, windowY + y, c);
            }

            if (x >= width - delta) {
                x = delta;
                y++;
            }

            if (y >= height - delta)
                break;
        }
    }
}
