package ge.rbg.fight.window;

import ge.rbg.fight.display.CLI;

public abstract class AbstractWindow {
    int windowX;
    int windowY;
    int width;
    int height;
    int border;
    int padding;

    AbstractWindow(int windowX, int windowY, int width, int height) {
        this.width = width;
        this.height = height;
        this.windowX = windowX;
        this.windowY = windowY;
    }

    void draw(CLI display) {
        display.outLine(windowX, windowY, windowX + width - 1, windowY + height - 1);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    int getBorder() {
        return border;
    }

    void setBorder(int border) {
        this.border = border;
    }

    int getPadding() {
        return padding;
    }

    void setPadding(int padding) {
        this.padding = padding;
    }
}
