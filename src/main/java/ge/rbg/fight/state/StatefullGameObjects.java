package ge.rbg.fight.state;

import ge.rbg.fight.Controls;
import ge.rbg.fight.domain.AbstractPlayer;
import ge.rbg.fight.domain.Rat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class StatefullGameObjects implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Random rnd = new Random();

    private final int width;
    private final int height;

    private byte[] filling;
    private List<AbstractPlayer> gameObjects;

    private StatefullGameObjects(int width, int height) {
        this.width = width;
        this.height = height;

        filling = new byte[width * height];
        gameObjects = new ArrayList<>();
    }

    private static void vline(StatefullGameObjects map, int x, int y, int l, Controls.Direction d) {
        while (l-- >= 0 && y > 0 && y < map.height) {
            map.filling[getAnInt(map, x, y)] = 2;
            y += d.dy;
        }
    }

    private static int getAnInt(StatefullGameObjects map, int x, int y) {
        return y * map.width + x;
    }

    private static void hline(StatefullGameObjects map, int x, int y, int l, Controls.Direction d) {
        while (l-- >= 0 && x > 0 && x < map.width) {
            map.filling[getAnInt(map, x, y)] = 10;
            x += d.dx;
        }
    }

    private static void cross(StatefullGameObjects map) {
        int x = rnd.nextInt(map.width - 1);
        int y = rnd.nextInt(map.height - 1);
        int l = rnd.nextInt(11);

        Controls.Direction vertDir = rnd.nextInt(11) > 5 ? Controls.Direction.UP : Controls.Direction.DOWN;
        Controls.Direction horDir = rnd.nextInt(11) > 5 ? Controls.Direction.LEFT : Controls.Direction.RIGHT;

        hline(map, x, y, l, horDir);
        vline(map, x, y, l, vertDir);
    }

    public static StatefullGameObjects RANDOM() {
        StatefullGameObjects map = new StatefullGameObjects(110, 100);
        for (int i = 0; i < 5; i++) {
            cross(map);
        }

        for (int i = 0; i < 20; i++) {
            int x = rnd.nextInt(map.width);
            int y = rnd.nextInt(map.height);

            Rat rat = new Rat();
            rat.setxPos(x);
            rat.setyPos(y);

            map.getGameObjects().add(rat);
        }

        return map;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public List<AbstractPlayer> getGameObjects() {
        return gameObjects;
    }

    public boolean canMove(int x, int y, Controls.Direction d) {
        x += d.dx;
        y += d.dy;

        if (x >= width || y >= height || x < 0 || y < 0)
            return false;

        return get(x, y) == 0;
    }

    // FIXME: slow, use better data structure
    public List<AbstractPlayer> getObjectsAt(int x, int y) {
        return gameObjects.stream()
                .filter(c -> c.getyPos() == y && c.getxPos() == x)
                .collect(Collectors.toList());
    }

    private byte get(int x, int y) {
        assert(x < width && x >= 0);
        assert(y < height && y >= 0);
        return filling[getAnInt(StatefullGameObjects.this, x, y)];
    }

    public MapView view(int x, int y, int w, int h) {
        return new MapView(x, y, w, h);
    }

    public class MapView {
        final int vx, vy, viewWidth, viewHeight;
        private Collection<AbstractPlayer> objectsView;

        private MapView(int vx, int vy, int viewWidth, int viewHeight) {
            this.vx = vx;
            this.vy = vy;
            this.viewWidth = viewWidth;
            this.viewHeight = viewHeight;

            objectsView = gameObjects.stream().filter(this::inView)
                    .sorted((a, b) -> Integer.compare(a.getSortOrder(), b.getSortOrder()))
                    .collect(Collectors.toList());
        }

        private boolean inView(AbstractPlayer obj) {
            int x = obj.getxPos();
            int y = obj.getyPos();

            if (x >= vx && x < vx + viewWidth) {
                if (y >= vy && y < vy + viewHeight) {
                    return true;
                }
            }
            return false;
        }

        public int viewX(int realX) {
            return realX - vx;
        }

        public int viewY(int realY) {
            return realY - vy;
        }

        public int width() {
            return viewWidth;
        }

        public int height() {
            return viewHeight;
        }

        public Collection<AbstractPlayer> getObjects() {
            return objectsView;
        }

        public byte get(int x, int y) {
            return StatefullGameObjects.this.get(x + vx, y + vy);
        }
    }
}
