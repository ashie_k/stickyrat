package ge.rbg.fight.domain;

import ge.rbg.fight.Controls;

public class Player extends AbstractPlayer {
    private static final long serialVersionUID = 1L;

    // Singleton
    public static class Builder {
        public String name;
        public Fight fight;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder race(Fight fight) {
            this.fight = fight;
            return this;
        }
        public Player build() {
            return new Player(name, fight);
        }
    }

    private final String name;
    private final Fight fight;
    private int experience;

    public Player(String name, Fight fight) {
        super('P', Integer.MAX_VALUE);
        this.name = name;
        this.fight = fight;
    }

    public void move(Controls.Direction dir) {
        xPos += dir.dx;
        yPos += dir.dy;
    }

    public String getName() {
        return name;
    }

    public Fight getFight() {
        return fight;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public enum Fight {
        NINJA,
        KARATE
    }

}
