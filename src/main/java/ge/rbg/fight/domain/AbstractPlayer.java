package ge.rbg.fight.domain;

import java.io.Serializable;

public abstract class AbstractPlayer implements Serializable {
    private char playerSymbol; // X or " or P
    protected int xPos;
    protected int yPos;
    private final int sortOrder;

    public AbstractPlayer(char playerSymbol, int sortOrder) {
        this.playerSymbol = playerSymbol;
        this.sortOrder = sortOrder;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public char getPlayerSymbol() {
        return playerSymbol;
    }

    public int getSortOrder() {
        return sortOrder;
    }
}
