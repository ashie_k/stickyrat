package ge.rbg.fight.domain;

public class Rat extends AbstractPlayer {
    private static final long serialVersionUID = 1L;

    public Rat() {
        super('R', 10);
    }
}
