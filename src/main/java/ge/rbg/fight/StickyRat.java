package ge.rbg.fight;

import ge.rbg.fight.display.CLI;
import ge.rbg.fight.display.CLIImpl;
import ge.rbg.fight.service.Actions;
import ge.rbg.fight.service.GameActions;
import ge.rbg.fight.service.MenuActions;
import ge.rbg.fight.utils.SleepBetweenFrames;

import java.io.IOException;

public class StickyRat {
    public static final StickyRat INSTANCE = new StickyRat();
    private static boolean quit = false;
    private Actions actions;

    private StickyRat() {
    }

    private static int readKey() {
        int key = 0;
        try {
            int cnt = System.in.available(); // Not to get stuck until a input is received
            if (cnt > 0)
                key = System.in.read();
        } catch (IOException e) {
            throw new RuntimeException("Unhandled exception", e);
        }
        return key;
    }

    public static void main(String[] args) throws IOException {
        CLI display = new CLIImpl(79, 25);

        GameActions.INSTANCE = new GameActions(display);
        StickyRat.INSTANCE.setActions(new MenuActions(display));

        SleepBetweenFrames sleepBetweenFrames = new SleepBetweenFrames(10);
        // Heart of the game
        while (!quit) {
            Actions a = StickyRat.INSTANCE.actions;
            processKey(a);

                if (sleepBetweenFrames.isReadyForNextFrame()) {
                nextFrame(display, a);
                sleepBetweenFrames.onFrame();
            }
        }
    }

    private static void nextFrame(CLI display, Actions a) {
        display.clear();
        a.print();
        display.flush();
    }

    private static void processKey(Actions a) {
        int key = readKey();
        if (key > 0)
            a.onKeyDown(key);
    }

    public void setActions(Actions a) {
        this.actions = a;
        a.start();
    }

    public void quit() {
        quit = true;
    }
}
