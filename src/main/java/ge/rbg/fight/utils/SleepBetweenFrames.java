package ge.rbg.fight.utils;

import java.util.concurrent.TimeUnit;

public class SleepBetweenFrames {
    private final double framesPS;
    private long t;

    public SleepBetweenFrames(double framesPS) {
        this.framesPS = framesPS;
    }

    public boolean isReadyForNextFrame() {
        return (long) (TimeUnit.SECONDS.toNanos(1) / framesPS - (System.nanoTime() - t)) < 0;
    }

    public void onFrame() {
        t = System.nanoTime();
    }
}
