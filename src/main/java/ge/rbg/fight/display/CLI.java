package ge.rbg.fight.display;

public interface CLI {

    int getWidth();

    int getHeight();

    void print(int x, int y, char symbol);

    void outLine(int x1, int y1, int x2, int y2);

    void fillArea(int x1, int y1, int x2, int y2);

    void clear();

    void flush();
}
