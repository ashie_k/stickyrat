package ge.rbg.fight.display;

import java.io.IOException;
import java.util.Arrays;

public class CLIImpl implements CLI {

    private static final String ANSI_CLEAR_SCREEN = "\u001B[2J";
    private static final String ANSI_MOVE_TOP_LEFT = "\u001B[0;0H";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";

    private final int height;
    private final int width;

    public final char[] areaCoordinates;
    private long lastCursor;

    public CLIImpl(int width, int height) {
        this.height = height;
        this.width = width;
        this.areaCoordinates = new char[width * height];
        clear();
    };

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public void print(int x, int y, char symbol) {
        areaCoordinates[y * width + x] = symbol;
    }

    private void horizontalLine(int x1, int height, int y) {
        int ptr = y * this.width + x1;
        while (height-- >= x1) {
            areaCoordinates[ptr++] = '~';
        }
    }

    private void verticalLine(int x, int y1, int width) {
        int ptr = y1 * this.width + x;
        while (width-- >= y1) {
            areaCoordinates[ptr] = '#';
            ptr += this.width;
        }
    }

    @Override
    public void outLine(int x1, int y1, int width, int height) {

        verticalLine(x1, y1 + 1, height - 1); // (0, 0-h)
        verticalLine(width, y1 + 1, height - 1); // (w, 0-h)

        horizontalLine(x1 + 1, width - 1, y1); // (0-w, 0)
        horizontalLine(x1 + 1, width - 1, height); // (0-w, h)

        // Corners
        print(x1, y1, '%');
        print(width, y1, '%');
        print(x1, height, '%');
        print(width, height, '%');
    }

    @Override
    public void fillArea(int x1, int y1, int width, int height) {

        while (x1 <= width) {
            int y = y1;
            while (y <= height) {
                print(x1, y++, ' ');
            }
            x1++;
        }
    }

    public void flush() {
        int newCrc = Arrays.hashCode(areaCoordinates);
        if (newCrc == lastCursor)
            return;

        lastCursor = newCrc; // Not to repeat the frames, retail the same frame if hashcode matches

        if (System.getProperty("os.name").startsWith("Windows")) {
            try {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } catch (InterruptedException | IOException e) {
                throw new RuntimeException("Unhandled exception", e);
            }
        }

        drawOnCLI();
    }

    private void drawOnCLI() {
        int ptr = 0;
        while (ptr < width * height) {
            // Print the outline and game to console
            System.out.print(areaCoordinates[ptr++]);
            if (ptr % width == 0)
                System.out.println();
        }
    }

    public void clear() {
        Arrays.fill(areaCoordinates, ' ');
    }
}
