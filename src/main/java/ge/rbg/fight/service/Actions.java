package ge.rbg.fight.service;

import ge.rbg.fight.display.CLI;

public abstract class Actions {
    final protected CLI display;

    public Actions(CLI display) {
        this.display = display;
    }

    abstract public void onKeyDown(int key);

    abstract public void print();

    public abstract void start();

}
