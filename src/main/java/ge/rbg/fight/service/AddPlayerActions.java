package ge.rbg.fight.service;

import ge.rbg.fight.Controls;
import ge.rbg.fight.display.CLI;
import ge.rbg.fight.domain.Player;
import ge.rbg.fight.utils.SleepBetweenFrames;
import ge.rbg.fight.window.Input;
import ge.rbg.fight.window.TextWindow;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class AddPlayerActions extends Actions {
    final List<String> phrases = Arrays.asList(
            "Are you ready to kill some rats??",
            "This is going to require some skills, may be you should watch some youtube videos on how to fight",

            "Are you game??",
            "what should be your name?",
            "%name",

            "Take your choices to fight:",
            Arrays.asList(Player.Fight.values()).stream()
                    .map(r -> " - " + r.name() + "(" + r.name().substring(0, 1).toLowerCase() + ")")
                    .collect(Collectors.joining("\n")),
            "%fight",

            "Okay, time for some action!!",
            "There will be a lot of rats be quick!!",
            "When you are on top of the rat, you will be given options to use Knife/Punch/Stick!!",
            "Choose what you like!",
            "Press Enter to continue!",
            "%enter"
    );

    int idx;

    private TextWindow dialog;
    private SleepBetweenFrames sleepBetweenFrames;
    private Input input;

    private Player.Builder player;

    public AddPlayerActions(CLI display) {
        super(display);
    }

    @Override
    public void onKeyDown(int key) {
        if (input == null)
            return;

        if (key == Controls.ENTER) {
            input.onEnter();
        } else {
            dialog.deleteLast(input.length());
            input.onKeyDown(key);
            dialog.append(input.value());
        }
    }

    @Override
    public void start() {
        dialog = new TextWindow(0, 0, display.getWidth() - 1, display.getHeight() - 1);
        sleepBetweenFrames = new SleepBetweenFrames(0.5);
        player = new Player.Builder();
        idx = 0;
    }

    //this.getClass().getDeclaredMethod(text.substring(1)).invoke(this);
    private void name() {
        input = new Input(10, Pattern.compile("[a-z]"), () -> {
            if (input.value().isEmpty())
                return;

            player.name(input.value());
            dialog.append("\n");
            input = null;
        });
    }

    //this.getClass().getDeclaredMethod(text.substring(1)).invoke(this);
    private void fight() {
        input = new Input(1, Pattern.compile("[a-z]"), () -> {
            if (input.value().isEmpty())
                return;

            for (Player.Fight fight : Player.Fight.values()) {
                if (fight.name().startsWith(input.value().toUpperCase())) {
                    player.race(fight);
                }
            }

            dialog.append("\n");
            if (player.fight != null) {
                input = null;
            } else {
                dialog.append("what?\n");
                input.clear();
            }
        });
    }

    private void newGame() {
        GameActions.INSTANCE.newGame(player.build());
    }

    //this.getClass().getDeclaredMethod(text.substring(1)).invoke(this);
    private void enter() {
        input = new Input(0, Pattern.compile(""), this::newGame);
    }

    @Override
    public void print() {
        if (input == null && sleepBetweenFrames.isReadyForNextFrame()) {
            String text = phrases.get(idx++);
            if (text.startsWith("%")) {
                try {
                    this.getClass().getDeclaredMethod(text.substring(1)).invoke(this);
                } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    throw new RuntimeException("Unhandled exception", e);
                }
            } else {
                if (player.name != null)
                    text = text.replace("[name]", player.name);
                dialog.append(text);
                dialog.append("\n");
                sleepBetweenFrames.onFrame();
            }
        }
        dialog.draw(display);
    }
}
