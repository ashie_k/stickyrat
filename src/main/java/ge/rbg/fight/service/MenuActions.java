package ge.rbg.fight.service;

import ge.rbg.fight.StickyRat;
import ge.rbg.fight.display.CLI;
import ge.rbg.fight.window.MenuWindow;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class MenuActions extends Actions {
    private MenuWindow menuWindow;
    private LoadActions loadActivity;

    public MenuActions(CLI display) {
        super(display);
    }

    @Override
    public void onKeyDown(int key) {
        if (loadActivity != null)
            loadActivity.onKeyDown(key);
        else
            menuWindow.onKeyDown(key);
    }

    @Override
    public void start() {
        List<Menu.Option> choices = Arrays.asList(
                new Menu.Option("New Game", () -> StickyRat.INSTANCE.setActions(new AddPlayerActions(display))),
                new Menu.Option("Load Saved Game", () -> setLoading(true)),
                new Menu.Option("Quit", StickyRat.INSTANCE::quit)
        );

        menuWindow = new MenuWindow(new Menu(choices));
    }

    private void setLoading(boolean value) {
        assert (loadActivity != null != value);

        if (value) {
            loadActivity = new LoadActions(display);
            loadActivity.start();
        } else {
            loadActivity = null;
        }
    }

    @Override
    public void print() {
        if (loadActivity != null)
            loadActivity.print();
        else
            menuWindow.draw(display);
    }

    public class LoadActions extends Actions {
        private MenuWindow loadMenuWindow;

        public LoadActions(CLI display) {
            super(display);
        }

        @Override
        public void onKeyDown(int key) {
            loadMenuWindow.onKeyDown(key);
        }

        @Override
        public void start() {
            Path cwd = FileSystems.getDefault().getPath("");
            List<Menu.Option> choices;
            try {
				// Loading a saved game, else choices will be empty
                choices = Files.find(cwd, 1, (path, attr) -> String.valueOf(path).endsWith(".ser"))
                        .map(f -> new Menu.Option(f.getFileName().toString(), () -> GameActions.INSTANCE.loadSaveFile(f)))
                        .collect(Collectors.toList());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Menu menu;
            if (choices.isEmpty())
                menu = new Menu(Collections.singletonList(new Menu.Option("NO SAVED GAME", () -> setLoading(false))));
            else
                menu = new Menu(choices);

            loadMenuWindow = new MenuWindow(menu);
        }

        @Override
        public void print() {
            loadMenuWindow.draw(display);
        }
    }

	public static class Menu {
		private List<Option> choices = new ArrayList<>();

		public int size() {
			return choices.size();
		}

		public List<Option> getChoices() {
			return choices;
		}

		public Menu(List<Option> choices) {
			this.choices = choices;
		}

		public static class Option {
			public String title;
			public Runnable action;
			public Supplier<Menu> next;

			public Option(String title, Runnable action) {
				this.title = title;
				this.action = action;
			}
		}
	}
}
