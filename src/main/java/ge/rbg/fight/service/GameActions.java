package ge.rbg.fight.service;

import ge.rbg.fight.Controls;
import ge.rbg.fight.StickyRat;
import ge.rbg.fight.display.CLI;
import ge.rbg.fight.domain.AbstractPlayer;
import ge.rbg.fight.domain.KilledRat;
import ge.rbg.fight.domain.Player;
import ge.rbg.fight.domain.Rat;
import ge.rbg.fight.state.StatefullGameObjects;
import ge.rbg.fight.window.Input;
import ge.rbg.fight.window.MapWindow;
import ge.rbg.fight.window.TextWindow;

import java.io.*;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Consumer;
import java.util.regex.Pattern;

public class GameActions extends Actions {
    private static final long SAVE_VERSION = 1L;
	private static final Random rnd = new Random();
    public static GameActions INSTANCE;
    private MapWindow mapWindow;
    private StatefullGameObjects map;
    private TextWindow stats;
    private PauseMenuActions pauseMenuActions;
    private FightActions fightActivity;
    Player player;

    public GameActions(CLI display) {
        super(display);
    }

    private void movePlayer(Controls.Direction d) {
        if (map.canMove(player.getxPos(), player.getyPos(), d)) {
            player.move(d);
            gainExperience(1);
            checkFight();
            mapWindow.scrollWithPlayer(player);
        }
    }

    private void gainExperience(int cnt) {
        player.setExperience(Math.max(0, player.getExperience() + cnt));
    }

    private void checkFight() {
        List<AbstractPlayer> objects = map.getObjectsAt(player.getxPos(), player.getyPos());
        if (objects.size() > 1) {
            Optional<AbstractPlayer> rat = objects.stream().filter(c -> c instanceof Rat).findAny();
            if (rat.isPresent()) {
                setFighting(rat.get());
            }
        }
    }

    private boolean isPaused() {
        return pauseMenuActions != null;
    }

    void setPaused(boolean p) {
        assert (isPaused() != p);

        if (p) {
            pauseMenuActions = new PauseMenuActions(display);
            pauseMenuActions.start();
        } else {
            pauseMenuActions = null;
        }
    }

    private boolean isFighting() {
        return fightActivity != null;
    }

    private void setFighting(AbstractPlayer f) {
        assert (isFighting() || f == null);

        if (f != null) {
            fightActivity = new FightActions(display, win -> {
                if (win) {
                    KilledRat dead = new KilledRat();
                    dead.setxPos(f.getxPos());
                    dead.setyPos(f.getyPos());

                    map.getGameObjects().add(dead);

                    int dx = rnd.nextInt(11) * (rnd.nextInt(11) > 5 ? 1 : -1);
                    int dy = rnd.nextInt(11) * (rnd.nextInt(11) > 5 ? 1 : -1);

                    f.setxPos(this.adjust(f.getxPos() + dx, 0, map.getWidth() - 1));
                    f.setyPos(this.adjust(f.getyPos() + dy, 0, map.getHeight() - 1));

                    gainExperience(10);
                } else {
                    gainExperience(-10);
                }

                fightActivity = null;
            });
            fightActivity.start();
        }
    }

	public static int adjust(int x, int min, int max) {
		if (x < min) return min;
		if (x > max) return max;
		return x;
	}

	@Override
    public void onKeyDown(int key) {
        if (isPaused()) {
            pauseMenuActions.onKeyDown(key);
            return;
        }

        if (key == Controls.QUIT) {
            setPaused(true);
            return;
        }

        if (isFighting()) {
            fightActivity.onKeyDown(key);
            return;
        }

        switch (key) {
            case Controls.DOWN:
                movePlayer(Controls.Direction.DOWN);
                break;

            case Controls.UP:
                movePlayer(Controls.Direction.UP);
                break;

            case Controls.LEFT:
                movePlayer(Controls.Direction.LEFT);
                break;

            case Controls.RIGHT:
                movePlayer(Controls.Direction.RIGHT);
                break;
        }
    }

    @Override
    public void print() {
        stats.setText(String.format("%s %s\nExperience: %07d", player.getFight().name(), player.getName(), player.getExperience()));

        mapWindow.draw(display);
        stats.draw(display);

        if (isFighting())
            fightActivity.print();

        if (isPaused())
            pauseMenuActions.print();
    }

    @Override
    public void start() {
        stats = new TextWindow(0, display.getHeight() - 1 - 6, display.getWidth(), 6);
        mapWindow = new MapWindow(0, 0, display.getWidth(), display.getHeight() - stats.getHeight());
    }

    void newGame(Player p) {
        StickyRat.INSTANCE.setActions(GameActions.INSTANCE);

        this.player = p;

        map = StatefullGameObjects.RANDOM();
        map.getGameObjects().add(p);
        mapWindow.setMap(map);
    }

    void loadSaveFile(Path path) {
        StickyRat.INSTANCE.setActions(this);

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path.toFile()))) {
            long version = ois.readLong();

            if (version != SAVE_VERSION) {
                throw new RuntimeException("Unknown save format");
            }

            map = (StatefullGameObjects) ois.readObject();
            mapWindow.setMap(map);
            player = (Player) map.getGameObjects().stream().filter(c -> c.getClass().equals(Player.class)).findFirst().get();
            mapWindow.centerAt(player);
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException("Unhandled exception", e);
        }
    }

    void saveToFile(Path path) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path.toFile()))) {
            oos.writeLong(1);
            oos.writeObject(map);
        } catch (IOException e) {
            throw new RuntimeException("Unhandled exception", e);
        }
    }

    public static class FightActions extends Actions {

		private final Consumer<Boolean> callback;
		private TextWindow dialog;
		private Input input;

		public FightActions(CLI display, Consumer<Boolean> callback) {
			super(display);
			this.callback = callback;
		}

		@Override
		public void onKeyDown(int key) {
			if (input == null)
				return;

			input.onKeyDown(key);
			if (input.length() > 0)
				input.onEnter();
		}

		@Override
		public void print() {
			dialog.draw(display);
		}

		private void reset() {
			dialog.setText("There you go!! Hit!! \n" +
					"Knife Punch Stick [k/p/s]: ");
			input.clear();
		}

		private Input enter(Input prev) {
			return new Input(0, Pattern.compile(""), () -> {
				input = prev;
				reset();
			});
		}

		@Override
		public void start() {
			int x = display.getWidth() / 4;
			int y = display.getHeight() / 4;
			dialog = new TextWindow(x, y, display.getWidth() - x * 2, display.getHeight() - y * 2);

			input = new Input(1, Pattern.compile("[kp]"), () -> {
				if (input.length() == 0)
					return;

				RockPaper pen = RockPaper.roll();
				RockPaper you = RockPaper.fromChar(input.value());

				dialog.append(you + " --- " + pen + "\n");
				int cmp = you.compare(pen);
				if (cmp == 0) {
					dialog.append("Draw! Press enter to fight again!");
					input = enter(input);
				} else if (cmp > 0) {
					dialog.append("\nWin!");
					input = finish(true);
				} else {
					dialog.append("\nLose!");
					input = finish(false);
				}
			});
			reset();
		}

		private Input finish(boolean b) {
			return new Input(0, Pattern.compile(""), () -> callback.accept(b));
		}

		public enum RockPaper {
			KNIFE, PUNCH, STICK;

			public static RockPaper roll() {
				return RockPaper.values()[rnd.nextInt(RockPaper.values().length)];
			}

			public static RockPaper fromChar(String s) {
				for (int i = 0; i < RockPaper.values().length; i++) {
					RockPaper rockPaper = RockPaper.values()[i];

					if (rockPaper.name().startsWith(s.toUpperCase()))
						return rockPaper;
				}

				throw new RuntimeException("wat?");
			}

			public int compare(RockPaper other) {
				if (this == other)
					return 0;
				else if (this == KNIFE)
					return other == PUNCH ? -1 : 1;
				else if (this == PUNCH)
					return other == STICK ? -1 : 1;
				else if (this == STICK)
					return other == KNIFE ? -1 : 1;
				else
					throw new RuntimeException("wat?");
			}
		}
	}
}

