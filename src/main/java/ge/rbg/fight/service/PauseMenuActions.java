package ge.rbg.fight.service;

import ge.rbg.fight.Controls;
import ge.rbg.fight.StickyRat;
import ge.rbg.fight.display.CLI;
import ge.rbg.fight.window.Input;
import ge.rbg.fight.window.MenuWindow;
import ge.rbg.fight.window.TextWindow;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class PauseMenuActions extends Actions {
	private MenuWindow menuWindow;
	private SaveActions saveActivity;

	public PauseMenuActions(CLI display) {
		super(display);
	}

	@Override
	public void onKeyDown(int key) {
		if (isSaving()) {
			saveActivity.onKeyDown(key);
			return;
		}

		menuWindow.onKeyDown(key);
	}

	@Override
	public void start() {
		List<MenuActions.Menu.Option> choices = Arrays.asList(
				new MenuActions.Menu.Option("Resume", () -> GameActions.INSTANCE.setPaused(false)),
				new MenuActions.Menu.Option("Save", () -> setSaving(true)),
				new MenuActions.Menu.Option("Quit", StickyRat.INSTANCE::quit)
		);

		menuWindow = new MenuWindow(new MenuActions.Menu(choices));
	}

	@Override
	public void print() {
		if (isSaving()) {
			saveActivity.print();
		} else {
			menuWindow.draw(display);
		}
	}

	private boolean isSaving() {
		return saveActivity != null;
	}

	private void setSaving(boolean s) {
		assert (isSaving() != s);

		if (s) {
			saveActivity = new SaveActions(display);
			saveActivity.start();
		} else {
			saveActivity = null;
		}
	}

	public class SaveActions extends Actions {
		private TextWindow dialog;
		private Input input;

		public SaveActions(CLI display) {
			super(display);
		}


		@Override
		public void onKeyDown(int key) {
			if (input == null)
				return;

			if (key == Controls.ENTER) {
				input.onEnter();
			} else {
				dialog.deleteLast(input.length());
				input.onKeyDown(key);
				dialog.append(input.value());
			}
		}

		@Override
		public void start() {
			int x = display.getWidth() / 5;
			int y = display.getHeight() / 5;
			dialog = new TextWindow(x, y, display.getWidth() - x * 2, display.getHeight() - y * 2);

			Path savePath = FileSystems.getDefault().getPath(GameActions.INSTANCE.player.getName() + ".ser");
			dialog.setText("Save your adventure in " + savePath.toString() + "? (y/n)");
			input = new Input(1, Pattern.compile("[yn]"), () -> {
				if (input.value().equals("y")) {
					GameActions.INSTANCE.saveToFile(savePath);
				}
				setSaving(false);
			});
		}

		@Override
		public void print() {
			dialog.draw(display);
		}
	}
}
