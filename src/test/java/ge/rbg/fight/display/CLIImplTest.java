package ge.rbg.fight.display;

import ge.rbg.fight.state.StatefullGameObjects;
import ge.rbg.fight.window.MapWindow;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;

import static javafx.beans.binding.Bindings.when;
import static org.junit.Assert.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

public class CLIImplTest {
	@Mock
	CLI cli;

	@Before
	public void setUp() {
		initMocks(this);
		cli = new CLIImpl(10, 10);
	}

	@Test
	public void print() throws Exception {
		CLI cli = new CLIImpl(10,10);
		cli.print(3, 3, '$');
		assertEquals(((CLIImpl) cli).areaCoordinates[33], '$');
	}

	@Test
	public void fillArea() throws Exception {

	}

	@Test
	public void flush() throws Exception {

	}

	@Test
	public void clear() throws Exception {

	}

	@Test
	public void testAll() {
		CLIImpl ds = new CLIImpl(20, 20);
		ds.print(3, 3, 'О');
        ds.print(3, 5, 'О');
        ds.outLine(0, 0, ds.getWidth() - 1, ds.getHeight() - 1);
		ds.flush();
	}

	@Test
	public void testMap() {
		CLIImpl ds = new CLIImpl(20, 20);
		StatefullGameObjects map = StatefullGameObjects.RANDOM();

		MapWindow mapWindow = new MapWindow(0, 0, 15, 15);
		mapWindow.setMap(map);

		mapWindow.draw(ds);
		ds.flush();
	}

}