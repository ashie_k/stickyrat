package ge.rbg.fight.service;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FightTest {
	@Test
	public void compare() throws Exception {
		String[] wins = new String[]{"kp", "ps", "sk"};

		for (String win : wins) {
			GameActions.FightActions.RockPaper r1 = GameActions.FightActions.RockPaper.fromChar(win.substring(0, 1));
			GameActions.FightActions.RockPaper r2 = GameActions.FightActions.RockPaper.fromChar(win.substring(1, 2));

			assertFalse(win, r1.compare(r2) > 0);
			assertFalse(win, r2.compare(r1) < 0);
			assertTrue(win, r1.compare(r1) == 0);
		}
	}
}
