package ge.rbg.fight.utils;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.AdditionalMatchers.gt;
import static org.mockito.AdditionalMatchers.lt;

/**
 * Created by asish.kumar on 12-06-2017.
 */
public class SleepBetweenFramesTest {
	@Test
	public void isReadyForNextFrame() throws Exception {
		SleepBetweenFrames sleepBetweenFrames = new SleepBetweenFrames(10);
		assertTrue(sleepBetweenFrames.isReadyForNextFrame());
		sleepBetweenFrames.onFrame();
		assertFalse(sleepBetweenFrames.isReadyForNextFrame());
	}
	
}